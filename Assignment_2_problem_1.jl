### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# ╔═╡ 742bff39-994e-455e-ae80-abf0b53561bd
using DataStructures

# ╔═╡ 371e060a-44f0-440d-a840-3db2a85f9f2e


# ╔═╡ de1968bf-8597-40c5-a830-0c13c9fecc2a
#By Shekupa Johannes

# ╔═╡ 4bb16581-7791-4fad-acfc-374246a02e31
md"# ASSIGNMENT 2 "

# ╔═╡ be163483-4d05-4507-a0ee-437badd1cd7a
md"## Problem 1"

# ╔═╡ d533a290-16ca-4d71-b390-df64ba4cbf7c
md"##### Consider a multi-storey building that serves as the office for Company X.
Consider an agent in charge of the logistics in the company. Once a week,
the agent collects parcels from all offices in the building and sends them to
their destination. Each floor has a number of contiguous offices, each one
with a specific room number.
The possible actions the agent can execute are defined as follows:

move east: (me), to move to the next office eastward;

move west: (mw), to move to the next office westward;

move up: (mu), to move to the floor up;

move down: (md), to move to the floor down;

collect: (co), to collect a single item from an office.

Additionally, note that the agent can only collect one item at a time and can
do so only if it is in the same office where the item is situated. Furthermore,
moving eastward (or westward) is possible if the destination office exists. In
the same vein, moving up or down is possible only if the destination floor
exists. Finally, the cost attached to each action is defined in Table 1.
Your task is to write a program in Julia that implements a search-based
problem-solving mechanism using the A∗ algorithm as a strategy.

As a heuristic function, you will apply 3 unit costs for each parcel not yet collected. A goal state is one where all parcels have been collected.
Your program will allow the user to provide a detailed configuration of the
problem at runtime. This includes the number of storeys, how many occupied offices per floor, the number of parcels in each office and the location
of the agent at the beginning.
"

# ╔═╡ 4f0a5919-77c0-43af-83c4-4e182203b154
@enum Action me  mw mu md co

# ╔═╡ 9c4bc553-03de-4f79-a58e-970e4e35a6da
struct 	Office
	hasAgent::Bool
	hasParcel::Bool
	
end

# ╔═╡ e530e9dc-fe79-465b-a04d-151c2270eb94
struct State
	down::Office
	downEast::Office
	downWest::Office
	up::Office
	upEast::Office
	upWest::Office
	
	
end

# ╔═╡ 63275d0d-d7a4-4f14-b2f1-79d148be8747
struct Node
	state::State
	parent::Union{Nothing, Node}
	action::Union{Nothing, Action}
end

# ╔═╡ b47f5697-d477-4de9-bb5d-f0c98181813f
function get_transitions(node::Node)
	state = node.state
	transitions = Dict{Action, Node}()
	
	if state.down.hasAgent
		if state.down.hasParcel
			transitions[me] = Node(
				State(Office(true, false),state.downEast, state.downWest, state.up, State.upWest, state.upEast),
				node,
				me
			)
		end
		transitions[co] = Node(
				State(Office(true, false),state.downEast, state.downWest, state.up, State.upWest, state.upEast),
				node,
				co
			)
		end
		transitions[mw] = Node(
			State(Office(false, state.downEast.hasParcel), 
				Office(true, state.downWest.hasParcel), 
				state.up, state.upEast, state.upWest),
			node,
			mw
		)
	transitions[co] = Node(
				State(Office(true, false), state.downWest, state.up, State.upWest, state.upEast),
				node,
				co
			)
		
			transitions[up] = Node(State(
				state.downWest, 
				state.downEast, 
				Office(true, false)),
				node,

				up
		)

			

		transitions[mw] = Node(State(
			Office(true, state.upWest.hasParcel),
			Office(false, state.downEast.hasParcel),
			state.upEast, state.upWest),
			node,
			mw
		)
	transitions[co] = Node(
				State(Office(true, false), state.upWest, state.upEast),
				node,
				co
			)
		transitions[me] = Node(State(
			state.up,
			Office(false, state.upWest.hasParcel),
			Office(true, state.upEast.hasParcel)),
			node,
			me
		)
	transitions[co] = Node(
				State(Office(true, false), state.upWest, state.down),
				node,
				co
			)
	
			transitions[md] = Node(State(
				state.downWest, 
				state.downEast, 
				Office(true, false)),
				node,

				md
			)

	return transitions
end

# ╔═╡ c1e53be3-cd49-4de5-82b0-a4109a92b842
function is_goal(state::State)
	return !state.down.hasParcel && !state.up.hasParcel
end

# ╔═╡ 7b8186b5-5563-411b-b04b-aa4631921f6c
function get_path(node::Node)
	path = [node]
	while !isnothing(node.parent)
		node = node.parent
		pushfirst!(path, node)
	end
	return path
end

# ╔═╡ 7f727538-eec2-4f52-8c33-d6ae933909e4
function solution(node::Node, explored::Array)
	path = get_path(node)
	actions = []
	for node in path
		if !isnothing(node.action)
			push!(actions, node.action)
		end
	end

	cost = length(actions)
	
	return cost, "Found $(length(actions)) step solution in $(length(explored)) steps: $(join(actions, " -> "))"
end

# ╔═╡ 56dc1acf-b043-40de-8f29-e2c5993542a5
function failure(message::String)
	return -1, message
end

# ╔═╡ 258284fb-9636-48eb-9c33-a5ab599cb1ac
initial_state = State(Office(false, true), Office(true, true), Office(false, true), Office(true, true), Office(true, true), Office(true, true))

# ╔═╡ 416952d9-6be3-4bc7-a708-71f398c703bf
md"### Depth-first Search"

# ╔═╡ 56e39de2-504d-4387-8927-c8761d2397bc
function depth_search(start::State)
	node = Node(start, nothing, nothing)
	if is_goal(node.state)
		return solution(node, "Agent is already at goal", [])
	end
	frontier = Queue{Node}()
	enqueue!(frontier, node)
	explored = []
	while true
		if isempty(frontier)
			return failure("Failed to find solution after exploring all nodes")
		end
		node = dequeue!(frontier)
		push!(explored, node.state)
		for (action, child) in get_transitions(node)
			if !(child in frontier) && !(child.state in explored)
				if is_goal(child.state)
					return solution(child, explored)
				end
				enqueue!(frontier, child)
			end
		end
	end
end

# ╔═╡ a4bca3c5-782d-4b89-880f-69bc1cbba600
depth_search(initial_state)

# ╔═╡ c4efc4cb-75cb-492b-8876-68ace9dd0aa6
md"### Breadth-first Search"

# ╔═╡ 0589b31f-172b-4a4a-8ff8-9fd743b18dad
function breadth_search(start::State)
	node = Node(start, nothing, nothing)
	if is_goal(node.state)
		return solution(node, "Agent is already at goal", [])
	end
	frontier = Stack{Node}()
	push!(frontier, node)
	explored = []
	while true
		if isempty(frontier)
			return failure("Failed to find solution after exploring all nodes")
		end
		node = pop!(frontier)
		push!(explored, node.state)
		for (action, child) in get_transitions(node)
			if !(child in frontier) && !(child.state in explored)
				if is_goal(child.state)
					return solution(child, explored)
				end
				push!(frontier, child)
			end
		end
	end
end

# ╔═╡ 9019a5cd-7612-4600-b114-599880e90ce7
breadth_search(initial_state)

# ╔═╡ 880d3fa6-d995-45d5-a243-5a15133a8a87
md"### Uniform-cost Search"

# ╔═╡ ed108ff3-cc09-4814-ac8b-1d41e4aadd57
function calculate_path_cost(node::Node)::Int
	return length(get_path(node))
end

# ╔═╡ 801921b7-3837-41a8-b6ab-e9145104762a
function uniform_cost_search(start::State)
	node = Node(start, nothing, nothing)
	if is_goal(node.state)
		return solution(node, "Agent is already at goal", [])
	end
	frontier = PriorityQueue{Node, Int}()
	enqueue!(frontier, node, calculate_path_cost(node))
	explored = []
	while true
		if isempty(frontier)
			return failure("Failed to find solution after exploring all nodes")
		end
		node = dequeue!(frontier)
		push!(explored, node.state)
		for (action, child) in get_transitions(node)
			if !(child in keys(frontier)) && !(child.state in explored)
				if is_goal(child.state)
					return solution(child, explored)
				end
				enqueue!(frontier, child, calculate_path_cost(child))
			end
		end
	end
end

# ╔═╡ 30b212e5-5bc2-481d-ab86-0cacd9dcc63a
uniform_cost_search(initial_state)

# ╔═╡ da85e73e-db22-47e8-a6d5-0bd23c201697
md"### A∗ Search"

# ╔═╡ 7e537a84-029d-4a07-8b80-a1aa0bd8db66
function a_star_cost(node::Node)
	return calculate_path_cost(node) + heuristic(node)
end

# ╔═╡ 51c36ebf-e793-4f1f-8e92-e28e37b13a19
function a_star_search(start::State)
	node = Node(start, nothing, nothing)
	if is_goal(node.state)
		return solution(node, "Agent is already at goal", [])
	end
	frontier = PriorityQueue{Node, Int}()
	enqueue!(frontier, node, a_star_cost(node))
	explored = []
	count = 0
	while true
		if isempty(frontier)
			return failure("Failed to find solution after exploring all nodes")
		end
		node = dequeue!(frontier)
		push!(explored, node.state)
		for (action, child) in get_transitions(node)
			if !(child in keys(frontier)) && !(child.state in explored)
				if is_goal(child.state)
					return solution(child, explored)
				end
				enqueue!(frontier, child, a_star_cost(child))
			end
		end
		if count > 100
			return failure("Timed out")
		end
	end
end

# ╔═╡ 0a08cd50-5978-49aa-adaf-efd29bdf4675
a_star_search(initial_state)
